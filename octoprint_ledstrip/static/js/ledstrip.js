/*
 * View model for OctoPrint-Ledstrip
 *
 * Author: Paul H.
 * License: MIT
 */
$(function () {
    function LedstripViewModel(parameters) {
        var self = this;

        self.controlViewModel = parameters[0];

        self.getAdditionalControls = function () {
            return [{
                    name: 'Light',
                    type: 'section',
                    layout: 'horizontal',
                    children: [{
                            command: '@ledstrip %(brightness)s',
                            name: 'On',
                            input: [{
                                    parameter: 'brightness',
                                    default: 255,
                                    slider: {
                                        min: 0,
                                        max: 255
                                    }
                                }
                            ]
                        },
                        {
                            command: '@ledstrip 0',
                            name: 'Off'
                        }
                    ]
                }
            ];
        }
    }

    self.send = function(brightness) {
        $.ajax({
            url: API_BASEURL + 'plugin/ledstrip',
            type: 'POST',
            dataType: 'json',
            data: Json.stringify({
                command: 'set_led',
                brightness: brightness
            }),
            content_type:'application/json; charset=UTF-8'
        });
    };

    OCTOPRINT_VIEWMODELS.push({
        construct: LedstripViewModel,
        dependencies: ["controlViewModel"],
        elements: []
    });
});
